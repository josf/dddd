require 'rails_helper'

RSpec.describe Month, type: :model do
  pending "add some examples to (or delete) #{__FILE__}"

  context "Getting set up" do
    it "should be a month object" do
      m = Month.new()
      expect(m).to be_a Month
    end
  end
  
  context "following month" do
    m = Month.new()

    it "should return a date object" do
      dt = m.following_month(Date.today())
      expect(dt).to be_a Date
    end

    it "should return the current month when the date is before the 10th" do
      dt = m.following_month(Date.new(2015, 5, 3))
      expect(dt.month).to eq 5
    end

    it "should return the next month when the date is after the 10th" do
      dt = m.following_month(Date.new(2015, 5, 11))
      expect(dt.month).to eq 6
    end

    it "should change the date to the tenth" do
      dt = m.following_month(Date.new(2015, 5, 2))
      expect(dt.day).to eq 10
    end
  end

  
end
