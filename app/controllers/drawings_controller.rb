# coding: utf-8
class DrawingsController < ApplicationController
  def index
    # needs to be filtered to avoid showing future drawings
    @drawings = Drawing.all
  end
  
  def show
    @drawing = Drawing.find(params[:id])
  end

  def new
    user = current_user
    if !user || user.nil?
      redirect_to login_path
    else 
      @drawing = Drawing.new
    end
  end

  def create
    user = current_user
    if !user || user.nil?
      redirect_to login_path
    else 
      @drawing = Drawing.new(drawing_params)
      @drawing[:user_id] = user[:id]

      # this creates the new month if it doesn't exist yet
      mth = Month.new
      month = mth.first_month_after_date!(
        user.drawing_must_be_after
      )
      @drawing[:month_id] = month[:id]
      
      if @drawing.save
        redirect_to root_url, notice: "Le dessin a été enregistré"
      else
        render "new"
      end
    end
  end

  def edit
    session_user = current_user
    if !session_user || session_user.nil?
      redirect_to login_path
    elsif session_user[:id] != params[:id] && session_user[:privilege] != "admin"
      redirect_to to action: "show",
                     id: params[:id],
                     notice: "Vous ne pouvez pas modifier ce dessin",
                     status: 401
    end

    @drawing = Drawing.find(params[:id])
  end

  def update
    session_user = current_user
    if !session_user || session_user.nil?
      redirect_to login_path
    elsif session_user[:id] != params[:id] && session_user[:privilege] != "admin"
      redirect_to to action: "show",
                     id: params[:id],
                     notice: "Vous ne pouvez pas modifier ce dessin",
                     status: 401
    end

    @drawing = Drawing.find(params[:id])

    if @drawing.update(drawing_params)
      redirect_to @drawing, notice: "Le dessin a été modifié"
    else
      redirect_to "edit", notice: "Il y a un souci avec les modifications"
    end

    
  end

  private

  def drawing_params
    params.require(:drawing).permit(
      :title, :picture, :month_id
    )
  end



  
end
