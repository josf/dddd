# coding: utf-8
class UsersController < ApplicationController

  def index
    @users = User.all
  end

            
  def show
    @showUser = User.find(params[:id])
  end

  def new # empty user object for forms
    user = current_user
    if !user || user.nil?
      redirect_to login_path, notice: "Vous devez vous connecter d'abord"
    elsif user[:privilege] != "admin"
      redirect_to action: "index", status: 401, notice: "Seuls les administrateurs peuvent ajouter de nouveaux dessinateurs."
    end

    @user = User.new
  end

  def create
    user = current_user
    if !user || user.nil?
      redirect_to login_path, notice: "Vous devez vous connecter d'abord"
    elsif user[:privilege] != "admin"
      redirect_to action: "index", status: 401, notice: "Seuls les administrateurs peuvent ajouter de nouveaux dessinateurs."
    end
    
    @user = User.new(user_params)

    if @user.save
      redirect_to root_url, notice: "Le compte dessinateur a été créé"
    else
      render "new"
    end
  end


  def edit
    session_user = current_user
    if session_user.nil? || !session_user
      redirect_to login_path, notice: "Vous devez d'abord vous connecter"
    elsif session_user[:id] != params[:id] && session_user[:privilege] != "admin"
      redirect_to action: "show",
                  id: params[:id],
                  notice: "Vous ne pouvez pas modifier le profile de cet utilisateur.",
                   status: 401
    end
    @user = User.find(params[:id])
  end

  def update
    session_user = current_user
    if session_user.nil? || !session_user
      redirect_to login_path, notice: "Vous devez d'abord vous connecter"
    elsif session_user[:id] != params[:id] && session_user[:privilege] != "admin"
      redirect_to action: "show",
                  id: params[:id],
                  notice: "Vous ne pouvez pas modifier le profile de cet utilisateur.",
                   status: 401
    end

    @user = User.find(params[:id])
    if session_user[:privilege] != "admin" && user_params[:privilege]
      user_params.delete(:privilege)
    end

    if @user.update(user_params)
      redirect_to @user, notice: "Le profil utilisateur a été modifié"
    else
      redirect_to "edit", notice: "Il y avait une erreur dans les modifications"
    end
  end

  
  private

  def user_params
    params.require(:user).permit(
      :email, :password, :password_confirmation, :firstname, :lastname, :privilege, :avatar
    )

  end
  
end
