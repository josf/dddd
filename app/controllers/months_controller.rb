# coding: utf-8
class MonthsController < ApplicationController

  def index
    @months = Month.where("startdate <= now()")
  end

  def show
    @month = Month.find(params[:id])
  end

  def latest
    @month = Month.where("startdate <= now()")
             .order("startdate asc")
             .first
    render :template => 'months/show'
  end

  def edit
    session_user = current_user
    if !session_user || session_user.nil?
      redirect_to login_path
    elsif session_user.privilege == "normal"
      redirect_to "index", :notice => "Seuls les admins peuvent éditer les mois"
    end
    @month = Month.find(params[:id])
  end

  def update
    session_user = current_user
    if !session_user || session_user.nil?
      redirect_to login_path
    elsif session_user.privilege == "normal"
      redirect_to "index", :notice => "Seuls les admins peuvent éditer les mois"
    end

    @month = Month.find(params[:id])
    if @month.update(months_params)
      redirect_to @month, :notice => "La modification a été prise en compte"
    else
      redirect_to "edit", :notice => "Il y a eu un problème"
    end
  end

  private

  def months_params
    params.require(:month).permit(
      :title, :message
    )
  end
  
end
