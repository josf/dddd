# coding: utf-8
class SessionsController < ApplicationController

  def new
  end

  def create
    user = User.find_by(email: params[:email])
    if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      redirect_to root_url, notice: "Vous êtes connecté(e)."
    else
      flash.now.alert = "Email ou mot de passe incorrect"
      render "new"
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url, notice: "Déconnexion"
  end
end
