class Drawing < ActiveRecord::Base
  belongs_to :user
  belongs_to :month
  validates :user_id, presence: true
  validates :title, presence: true
  has_attached_file :picture, :styles => {
                      :large => "600x600",
                      :medium => "400x400",
                      :thumb => "150x150",
                      :mini => "80x80"
                    },
                    :default_url => "/images/:style/missing.png"
  validates_attachment_content_type :picture, :content_type => /\Aimage\/.*\Z/
  validates_attachment_file_name :picture, :matches => [/png\Z/, /jpe?g\Z/]
end
