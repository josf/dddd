class User < ActiveRecord::Base
  has_many :drawings, dependent: :destroy
  has_many :months, through: :drawings
  has_secure_password
  validates :email, presence: true, uniqueness: true
  validates :firstname, presence: true
  validates :privilege, inclusion: ["admin", "normal"]
  
  has_attached_file :avatar, :styles => {
                      :medium => "300x300",
                      :thumb => "100x100"
                    },
                    :default_url => "/images/:style/faceless.png"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/
  validates_attachment_file_name :avatar, :matches => [/png\Z/, /jpe?g\Z/]


  # Returns the date /after which/ a new month can be assigned. If the
  # user already has drawings assigned to future months, this function
  # will return the date of the last of those months. If the user has
  # no future drawings, the function will just return the current date. 
  #
  # +return+ Date object
  def drawing_must_be_after
    # these months by definition already have drawings
    now = Date.today()
    latest_future_month = self.months.reject { |m|
      m.startdate < now
    }

    if (latest_future_month.empty?)
      Date.today()
    else
      latest_future_month.first[:startdate] + 1
    end
  end
  
end
