class Month < ActiveRecord::Base
  has_many :drawings
  has_many :users, through: :drawings
  validates :startdate, presence: true, uniqueness: true

  def following_month(dt)
    if (dt.day > 10)
      newmonth = dt >> 1 # add one month
    else
      newmonth = dt
    end
    Date.new(newmonth.year, newmonth.month, 10)
  end

  # creates a new month if necessary (thus "!")
  def first_month_after_date! (dt)
    latest = Month.order('startdate asc')
             .where('startdate >= ?', dt)
             .first

    if latest.nil?
      newmonth = Month.new({ :startdate => following_month(dt)})
      newmonth.save
      newmonth
    else
      latest
    end
  end

  
end
