#!/bin/bash

apt-get update
apt-get -y install curl

apt-get -y install postgresql
#apt-get -y install postgresql-server-dev-9.1
# ubuntu 14 uses 9.3
apt-get -y install postgresql-server-dev-9.3

apt-get -y install git
apt-get -y install vim


gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
curl -sSL https://get.rvm.io | bash -s stable

source /usr/local/rvm/scripts/rvm

rvm use --default --install "ruby-2.2.2"

gem install bundler

mkdir /var/www
chown vagrant /var/www
